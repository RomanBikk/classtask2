package characters;

import interfaces.IDead;
import interfaces.ICharHiting;
import monsters.Monster;

public abstract class CharacterClass implements ICharHiting, IDead {
    String name;
    int strength;
    int dexterity;
    int magic;
    int vitality;
    double life;
    double mana;
    double armour;
    double damage;

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getMagic() {
        return magic;
    }

    public int getVitality() {
        return vitality;
    }

    public double getDamage() {
        return damage;
    }

    public String getName() {
        return name;
    }

    public double getLife() {
        return life;
    }

    public double getMana() {
        return mana;
    }

    public double getArmour() {
        return armour;
    }


    public void setLife(double life) {
        this.life = life;
    }

    @Override
    public void hit(Monster monster, double damage) {
        if (monster.getArmour() < damage/2) {
            monster.setLife(monster.getLife() - damage);
        }
        else monster.setLife(monster.getLife() - damage/2);


    }
}
