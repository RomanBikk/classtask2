package characters;

import monsters.Monster;

public class Wizard extends CharacterClass {

    public Wizard(String name) {
        this.name = name;
        this.strength = 20;
        this.dexterity = 10;
        this.vitality = 15;
        this.magic = 35;
        this.life = vitality * 1.0;
        this.armour = dexterity * 1.5;
        this.mana = magic * 2.0;
    }
    public double getLife() {
        return life;
    }

    public void setLife(double life) {
        this.life = life;
    }


    @Override
    public void hit(Monster monster, double damage) {
        if (monster.getArmour() < damage/2) {
            monster.setLife(monster.getLife() - damage);
            System.out.println("Тиу!" + " бьет " + this.name);
        }
        else {
            monster.setLife(monster.getLife() - damage/2);
            System.out.println("Дзынь!" + " бьет " + this.name);
        }

    }

    @Override
    public boolean isDead() {
        return this.life <= 0;
    }
}
