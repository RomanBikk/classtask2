package characters;

import monsters.Monster;

public class Rogue extends CharacterClass {

    public Rogue(String name) {
        this.name = name;
        this.strength = 30;
        this.dexterity = 25;
        this.vitality = 20;
        this.magic = 25;
        this.life = vitality * 1.5;
        this.armour = dexterity * 2;
        this.mana = magic * 1.5;
    }

    public double getLife() {
        return life;
    }

    public void setLife(double life) {
        this.life = life;
    }


    @Override
    public void hit(Monster monster, double damage) {
        if (monster.getArmour() < damage/2) {
            monster.setLife(monster.getLife() - damage);
            System.out.println("Бам!" + " бьет " + this.name);
        }
        else {
            monster.setLife(monster.getLife() - damage/2);
            System.out.println("Дзынь!" + " бьет " + this.name);}

    }

    @Override
    public boolean isDead() {
        return this.life <= 0;
    }
}
