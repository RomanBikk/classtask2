package characters;

import monsters.Monster;

public class Warior extends CharacterClass {

    public Warior(String name) {
        this.name = name;
        this.strength = 35;
        this.dexterity = 20;
        this.vitality = 25;
        this.magic = 20;
        this.life = vitality * 2;
        this.armour = dexterity * 1.5;
        this.mana = magic * 1.0;
    }
    public double getLife() {
        return life;
    }

    public void setLife(double life) {
        this.life = life;
    }


    @Override
    public void hit(Monster monster, double damage) {
        if (monster.getArmour() < damage/2) {
            monster.setLife(monster.getLife() - damage);
            System.out.println("Хрясь!" + " бьет " + this.name);
        }
        else {
            monster.setLife(monster.getLife() - damage/2);
            System.out.println("Дзынь!" + " бьет " + this.name);
        }


    }

    @Override
    public boolean isDead() {
        return this.life <= 0;
    }
}
