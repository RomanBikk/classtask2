package interfaces;

import characters.CharacterClass;

public interface IMonsterStriking {
    void hit(CharacterClass hero);
}
