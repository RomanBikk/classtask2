package interfaces;

import monsters.Monster;

public interface ICharHiting {
    void hit(Monster monster, double damage);
}
