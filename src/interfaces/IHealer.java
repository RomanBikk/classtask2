package interfaces;

import monsters.Monster;

public interface IHealer {
    void heal(Monster monster);
}
