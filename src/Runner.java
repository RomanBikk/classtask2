import monsters.Devil;
import monsters.Monster;
import monsters.WarlockDevil;
import characters.CharacterClass;
import characters.Rogue;
import characters.Warior;
import characters.Wizard;

public class Runner {
    public static void main(String[] args) {
        CharacterClass[] heroes = new CharacterClass[3];
        heroes[0] = new Warior("Boolean1");
        heroes[1] = new Rogue("Cat");
        heroes[2] = new Wizard("Paganych99");
        Monster[] monsters = new Monster[3];
        monsters[0] = new Devil("Angry Red");
        monsters[1] = new Devil("Pinky Demon");
        monsters[2] = new WarlockDevil("Calm Blue");
        System.out.println("Засада!");
        while (!monsters[0].isDead() || !monsters[1].isDead() || !monsters[2].isDead()) {
            if (!monsters[0].isDead()) {
                monsters[0].hit(heroes[0]);
            }
            if (heroes[0].isDead()) {
                System.out.println(heroes[0].getName() + " погиб");
                break;
            } else heroes[0].hit(monsters[0], 18.0);
            if (!monsters[1].isDead()) {
                monsters[1].hit(heroes[1]);
            }
            if (heroes[1].isDead()) {
                System.out.println(heroes[1].getName() + " погиб");
                break;
            } else heroes[1].hit(monsters[1], 15.9);
            if (!monsters[2].isDead()) {
                monsters[2].hit(heroes[2]);
                monsters[2].heal(monsters[0]);
                monsters[2].heal(monsters[1]);
            }
            if (heroes[2].isDead()) {
                System.out.println(heroes[2].getName() + " погиб");
                break;
            } else heroes[2].hit(monsters[2], 13.7);

        }
    }
}
