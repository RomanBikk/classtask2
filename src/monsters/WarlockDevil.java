package monsters;


import characters.CharacterClass;

public class WarlockDevil extends Monster  {
    private String name;
    private double life;
    private double armour;
    private double damage;

    public double getDamage() {
        return damage;
    }

    public double getArmour() {
        return armour;
    }

    public String getName() {
        return name;
    }

    public double getLife() {
        return life;
    }
    public void setLife(double life) {
        this.life = life;
    }

    public WarlockDevil(String name) {
        this.name = name;
        this.life = 15;
        this.armour = 7;
        this.damage = 10;
    }

    @Override
    public boolean isDead() {
        if(this.life <= 0) {
            System.out.println(this.name + " мертв!");
            return true;
        }
        else return false;
    }

    @Override
    public void hit(CharacterClass hero) {
        if (hero.getArmour() < this.damage) {
            hero.setLife(hero.getLife() - this.damage);
            System.out.println("Бух!" + " бьет " + this.name);
        }
        else {
            hero.setLife(hero.getLife() - this.damage / 2);
            System.out.println("Дзынь!" + " бьет " + this.name);
        }
    }

    @Override
    public void heal(Monster monster) {
        if(Math.random()*10 > 4) {
            monster.setLife(monster.getLife() * 2);
            System.out.println("Взззиуууу вылечен " + monster.getName() );
        }
        else monster.setLife(monster.getLife() + 1);
        System.out.println("Пуф! Не получилось вылечить " + monster.getName());
    }
}
