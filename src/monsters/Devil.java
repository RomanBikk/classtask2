package monsters;

import characters.CharacterClass;

public class Devil extends Monster {
    private String name;
    private double life;
    private double armour;
    private double damage;

    public double getDamage() {
        return damage;
    }

    public double getArmour() {
        return armour;
    }

    public String getName() {
        return name;
    }

    public double getLife() {
        return life;
    }
    public void setLife(double life) {
        this.life = life;
    }

    public Devil(String name) {
        if(name.contains("Pinky")) {
            this.name = name;
            this.life = 20;
            this.armour = 15;
            this.damage = 25;
        }
        else {
        this.name = name;
        this.life = 12;
        this.armour = 8;
        this.damage = 10;}
    }

    @Override
    public boolean isDead() {
        if(this.life <= 0) {
            System.out.println(this.name + " мертв!");
            return true;}
            else return false;
    }

    @Override
    public void hit(CharacterClass hero) {
        if (hero.getArmour() < this.damage) {
            hero.setLife(hero.getLife() - this.damage);
            System.out.println("Бух!" + " бьет " + this.name);
        }
        else {hero.setLife(hero.getLife() - this.damage/2);
            System.out.println("Дзынь!" + " бьет " + this.name);}
    }
}
