package monsters;

import interfaces.IDead;
import interfaces.IHealer;
import interfaces.IMonsterStriking;

public abstract class Monster implements IDead, IMonsterStriking, IHealer {
    private String name;
    private double life;
    private double armour;
    private double damage;

    public String getName() {
        return name;
    }

    public double getLife() {
        return life;
    }

    public double getArmour() {
        return armour;
    }

    public double getDamage() {
        return damage;
    }
    public void setLife(double life) {
        this.life = life;
    }

    @Override
    public boolean isDead() {
        if(this.life <= 0) {
            System.out.println(this.name + " мертв!");
            return true;
        }
        else return false;
    }
    public void heal(Monster monster) {

    }
}
